<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Eventium
 */

get_header(); ?>

<section class="main col-sm-8 col-md-7">
	<?php get_template_part('parts/action-bar'); ?>
	<section class="section-home">
		<div class="row-fluid">
<?php
	$categorias = EM_Categories::get();
	$size_name = 'category-thumbnail';
	$size_thumbnail = get_image_sizes($size_name);
	foreach ($categorias as $categoria):
		$category_image_id = $categoria->get_image_id();
		$image_src = wp_get_attachment_image_src($category_image_id, $size_name)[0];
?>
			<div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 category-grid-item-wrapper">
				<div class="category-grid-item">
					<a href="<?php echo $categoria->get_url(); ?>">
						<h3><?php echo str_replace("-", "", $categoria->slug); ?></h3>
						<img src="<?php echo $image_src ?>" alt="<?php echo $categoria->slug; ?>-image" width="<?php echo $size_thumbnail['width']; ?>" height="<?php echo $size_thumbnail['height']; ?>">
					</a>
				</div>
			</div>
<?php
	endforeach;
?>
			<div class="clear"></div>
		</div>
	</section>

<?php get_footer(); ?>
