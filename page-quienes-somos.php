<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Eventium
 */

get_header(); ?>

<section class="main col-sm-8 col-md-7">
	<?php //get_template_part('parts/search-bar'); ?>
	<section class="page-content">
	
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>

			<?php endwhile; // end of the loop. ?>
			<div class="row">
				<div class="col-md-6 text-center perfil">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/landing/perfil_maria.png" width="120" alt="María José Serrano">
					<h4>María José Serrano Barea</h4>
					<p><a href="https://twitter.com/MaJoSe79" target="_blank"><i class="fa fa-twitter"></i></a>&nbsp;
					<a href="https://es.linkedin.com/in/mjserrano79" target="_blank"><i class="fa fa-linkedin-square"></i></a>&nbsp;
					<a href="https://www.facebook.com/mariajose.serrano.589" target="_blank"><i class="fa fa-facebook-official"></i></a><br>
					<a href="http://mjserrano.com" target="_blank"><i class="fa fa-globe">&nbsp;</i> mjserrano.com</a>
					</p>
				</div>
				<div class="col-md-6 text-center perfil">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/landing/perfil_rafa.png" width="120" alt="María José Serrano">
					<h4>Rafael Perelló Rowley</h4>
					<p><a href="https://twitter.com/rperellorow" target="_blank"><i class="fa fa-twitter"></i></a>&nbsp;
					<a href="https://es.linkedin.com/pub/rafael-perell%C3%B3-rowley/11/323/350" target="_blank"><i class="fa fa-linkedin-square"></i></a>
				</div>
			</div>
</section>


<?php get_sidebar(); ?>
<?php get_footer(); ?>
