<?php
  $fecha_anterior = "";
  $eventos = $this->eventos;
  $hoy = date('Y-m-d');
  $hoy_time = strtotime($hoy);
  foreach ($eventos as $i => $evento) :
    $image_src = wp_get_attachment_image_src(get_post_thumbnail_id($evento->post_id), 'event-thumbnail')[0];
    if ($evento->event_end_date && $evento->event_end_date != $evento->event_start_date && $evento->start < $hoy_time) {
      $nueva_fecha = $hoy;
    } else {
      $nueva_fecha = $evento->event_start_date;
    }
    $fecha = strtotime($nueva_fecha);
    $location = new EM_Location($evento->location_id);
?>
<?php
    if ($nueva_fecha != $fecha_anterior):
      if ($i && $nueva_fecha != $fecha_anterior):
?>
        </div><!-- .day-events -->
<?php
    endif;
?>
    <div class="day-events">
      <div class="full-date">
        <span class="the-day"><?php echo date_i18n('j', $fecha); ?></span>
        <span class="the-month"><?php echo date_i18n('F', $fecha); ?></span>
        <span class="the-year"><?php echo date_i18n('Y', $fecha); ?></span>
      </div>
<?php
    endif;
?>
      <div class="card">
        <div class="embed-responsive embed-responsive-card">
        <div class="row-fluid">
          <div class="col-xs-4 event-thumb">
            <a href="<?php echo get_post_permalink($evento->post_id); ?>">
              <img class="responsive" src="<?php echo $image_src; ?>" alt="">
            </a>
          </div>
          <div class="col-xs-8 event-summary">
            <h3 class="event-title">
              <a href="<?php echo get_post_permalink($evento->post_id); ?>"><?php echo $evento->name; ?></a>
            </h3>
<?php
  if ($evento->event_end_date && $evento->event_end_date != $evento->event_start_date):
?>
            <time datetime="<?php echo $evento->event_start_date . '' . $evento->event_start_time; ?>"><?php echo __('Desde', 'eventium') . " " . date_i18n('j F Y', $evento->start); ?><br><?php echo __('Hasta', 'eventium') . " " . date_i18n('j F Y', $evento->end); ?></time>
<?php
  else:
?>
            <time datetime="<?php echo $evento->event_start_date . '' . $evento->event_start_time; ?>"><?php echo date_i18n('j F Y - H:i', $evento->start); ?>h</time>
<?php
  endif;
?>
<?php
  if ($location && intval($location->location_id) > 0):
?>
            <span><?php echo $location->name; ?></span> - <span><?php echo $location->town; ?></span>
<?php
  endif;
?>
<!--             <div class="footer">
              <i class="fa fa-ticket"></i> <i class="fa fa-euro"></i> <span>+ 18</span>
            </div> -->
          </div>
          <div class="clear"></div>
        </div>
        </div>
      </div><!-- .card -->
<?php
    $fecha_anterior = $nueva_fecha;
  endforeach;
?>
    </div><!-- .day-events -->

