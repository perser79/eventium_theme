<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Eventium
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/assets/img/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/assets/img/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/assets/img/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/assets/img/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/assets/img/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/assets/img/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/assets/img/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/assets/img/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/assets/img/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri(); ?>/assets/img/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon-16x16.png">
		<link id="page_favicon" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon.ico" rel="icon" type="image/x-icon">
		<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">

<?php wp_head(); ?>
	</head>

	<body <?php body_class(); ?>>
		<div class="container">
			<?php if ( get_header_image() ) : ?>
			<div>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
					<img class="responsive" src="<?php header_image(); ?>" width="<?php echo esc_attr( get_custom_header()->width ); ?>" height="<?php echo esc_attr( get_custom_header()->height ); ?>" alt="">
				</a>
			</div>
			<?php endif; // End header image check. ?>
			<div class="navbar-header">
	      <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a href="#calendar-widget-sidebar" class="circular-action-btn red-btn pull-right visible-xs-inline-block" data-toggle="collapse" aria-expanded="false" aria-controls="calendar-widget-sidebar"><i class="flaticon-power106"></i></a>
<?php
	$url = home_url( '/' );
	if (is_front_page()):
?>
<?php if ( get_theme_mod( 'themeslug_logo_complete' ) ) : ?>
					<img class="logo-complete navbar-brand" src="<?php echo esc_url( get_theme_mod( 'themeslug_logo_complete' ) ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">		
<?php endif; ?>
<?php
	else:
?>
				<a href="javascript:history.back();" class="back-button visible-xs-inline-block visible-sm-inline-block navbar-brand">
					<i class="flaticon-go10"></i>
				</a>
				<a href="<?php echo $url; ?>" class="pull-left">
<?php if ( get_theme_mod( 'themeslug_logo_complete' ) && get_theme_mod( 'themeslug_logo_reduced' ) ) : ?>
					<img class="logo-complete visible-md-inline-block visible-lg-inline-block" src="<?php echo esc_url( get_theme_mod( 'themeslug_logo_complete' ) ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
					<img class="visible-xs-inline-block visible-sm-inline-block" src="<?php echo esc_url( get_theme_mod( 'themeslug_logo_reduced' ) ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
<?php elseif (get_theme_mod( 'themeslug_logo_complete' )): ?>
					<img class="logo-complete" src="<?php echo esc_url( get_theme_mod( 'themeslug_logo_complete' ) ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
<?php elseif ( get_theme_mod( 'themeslug_logo_reduced' ) ) : ?>
					<img src="<?php echo esc_url( get_theme_mod( 'themeslug_logo_reduced' ) ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
<?php endif; ?>
				</a>
<?php
	endif;
?>
	    </div>
			<?php
          wp_nav_menu( array(
              'menu'              => 'primary',
              'theme_location'    => 'primary',
              'depth'             => 2,
              'container'         => 'nav',
              'container_class'   => 'collapse navbar-collapse bs-navbar-collapse',
      				'container_id'      => 'bs-example-navbar-collapse-1',
              'menu_class'        => 'nav navbar-nav navbar-right',
              'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
              'walker'            => new wp_bootstrap_navwalker())
          );
      ?>
			<div class="row">
				<header class="col-sm-4 col-md-5">
					<div class="content-header-wrapper">
						<div class="claim visible-sm-block visible-md-block visible-lg-block">
							<h1 class="theme-claim"><?php echo get_theme_mod('theme_claim', 'encuentra, comparte, disfruta<br><span>los eventos de tu ciudad</span>'); ?></h1>
						</div>
						<?php get_sidebar('front'); ?>
					</div>
				</header>





