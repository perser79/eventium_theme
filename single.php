<?php
/**
 * The template for displaying all single posts.
 *
 * @package Eventium
 */

get_header(); ?>

<section class="main col-sm-8 col-md-7">
	<?php get_template_part('parts/search-bar'); ?>
	<section class="search-results-content">
	<?php while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'content', 'single' ); ?>
			<?php the_post_navigation(); ?>

			<?php
				// If comments are open or we have at least one comment, load up the comment template
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;
			?>

		<?php endwhile; // end of the loop. ?>
	</section>

<?php get_sidebar(); ?> 
<?php get_footer(); ?>
