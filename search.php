<?php
/**
 * The template for displaying search results pages.
 *
 * @package Eventium
 */

get_header(); ?>

<section class="main col-sm-8 col-md-7">
<?php
	$search_in = isset($_GET['search_in']) ? $_GET['search_in'] : 'post';
	if ($search_in == 'events'):
		// $error_reporting_level = error_reporting(E_ALL ^ E_NOTICE);
		echo '<pre style="display: none">';
		$eventos = EM_Events::get(array('search' => $_GET['s']));
	  echo '</pre>';
?>
  <?php get_template_part('parts/action-bar'); ?>
	<section class="section-category-events">
<?php
		if (count($eventos)):
			smk_get_template_part("list-events.php", array(
		    'eventos' => $eventos,
			));
		else:
?>
		<p>
			<?php echo __('No hemos encontrado ningún evento a partir de tu búsqueda', 'eventium') ?>
		</p>
<?php
		endif;
?>
	</section><!-- section-category -->
<?php
	else:
?>
	<?php get_template_part('parts/search-bar'); ?>
	<section class="search-results-content">
<?php
		$encontrados = false;
		if (have_posts()) :
			while (have_posts()) : the_post();
				if ($post->post_type == 'post'):
					$encontrados = true;
					get_template_part( 'content', 'search' );
				endif;
			endwhile; // end of the loop.
		endif;
		if (!$encontrados):
?>
			<p>
				<?php echo __('No hemos encontrado ninguna entrada a partir de tu búsqueda', 'eventium') ?>
			</p>
<?php
		endif;
?>
	</section>
<?php
	endif;
?>

<?php get_footer(); ?>
