<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Eventium
 */
?>
					</section><!-- .main -->
				</div><!-- .row -->
			</div><!-- .container -->
			<?php //get_sidebar('footer'); ?>
			<?php get_sidebar('landing'); ?>
		<footer class="pie">
			<span>Copyright <?php echo date('Y'); ?> - <?php bloginfo('name'); ?></span>
			<nav id="site-navigation" class="main-navigation" role="navigation">
				<?php wp_nav_menu( array( 'theme_location' => 'footer', 'menu_id' => 'footer-menu' ) ); ?>
			</nav><!-- #site-navigation -->
		</footer>
<?php wp_footer(); ?>

	</body>
</html>
