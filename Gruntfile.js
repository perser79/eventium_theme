module.exports = function(grunt) {

	// All configuration goes here
	grunt.initConfig({
    	pkg: grunt.file.readJSON('package.json'),

    	// Javascript concatenation
    	concat: {
        	dist: {
            	src: [
                    // 'bower_components/jquery/dist/jquery.js',
                	'node_modules/bootstrap/dist/js/bootstrap.js',
                	// 'src/js/jquery.json.js',
                	'src/js/custom.js',
            	],
            	dest: 'assets/js/app.js',
        	},
        	// html5: {
         //    	src: [
         //        	'src/js/html5shiv.js',
         //        	'src/js/respond.min.js',
         //    	],
         //    	dest: 'src/js/html5.js',
        	// }
    	},

    	// Javascript minification
    	uglify: {
        	dist: {
            	files: {
                	'assets/js/app.min.js': [
                    	'assets/js/app.js',
                	],
            	}
        	}
    	},

    	// CSS minification
    	cssmin: {
            	options: {
                	// Remove comments
                	keepSpecialComments: 0
            	},
            	// Everything in src/css is combined to a single assets/css/style.css
                target: {
            	    files: {
              	     'assets/css/style-pre.css': [
                        // 'assets/css/main.css',
                        'node_modules/bootstrap/dist/css/bootstrap.css',
                        'src/css/roboto.css',
                        'src/vendor/css/flaticon.css',
                        'src/css/custom.css'
                    ]
            	   }
                }
    	},

    	// LESS Compilation
    	less: {
        	build: {
            	files: {
                	'src/css/custom.css': [
                        'src/less/style.less',
                        'src/less/landing.less'
                    ]
            	}
        	}
    	},

    	// CSS autoprefixer
    	autoprefixer: {
        	options: {
          	     browsers: ['> 1%', 'last 2 versions', 'Firefox ESR', 'Opera 12.1', 'ie 8', 'ie 9']
        	},
        	your_target: {
            	src: 'assets/css/style-pre.css',
            	dest: 'assets/css/style.css'
        	}
    	},


    	watch: {
            options: {
                livereload: true
            },
        	less: {
            	files: ['src/less/*.less'],
            	tasks: ['less'],
        	},
        	css: {
            	files: ['src/css/*.css'],
            	tasks: ['cssmin', 'autoprefixer'],
        	},
        	js: {
            	files: ['src/js/custom.js'],
            	tasks: ['concat'],
        	}
    	},
        makepot: {
            target: {
                options: {
                    cwd: '',                          // Directory of files to internationalize.
                    domainPath: '/languages',                   // Where to save the POT file.
                    exclude: ['node_modules/.*', '.sass-cache/.*', 'plugins/.*', 'inc/CMB/.*', 'bower_components/.*'],                      // List of files or directories to ignore.
                    include: [],                      // List of files or directories to include.
                    mainFile: '',                     // Main project file.
                    potComments: 'eventium',                  // The copyright at the beginning of the POT file.
                    potFilename: 'eventium.pot',                  // Name of the POT file.
                    potHeaders: {
                        poedit: true,                 // Includes common Poedit headers.
                        'x-poedit-keywordslist': true // Include a list of all possible gettext functions.
                    },                                // Headers to add to the generated POT file.
                    processPot: null,                 // A callback function for manipulating the POT file.
                    type: 'wp-theme',                // Type of project (wp-plugin or wp-theme).
                    updateTimestamp: true,             // Whether the POT-Creation-Date should be updated without other changes.
                    updatePoFiles: false              // Whether to update PO files in the same directory as the POT file.
                }
            }
        }
	});

	// Tell Grunt we plan to use these plugins.
	// grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-less');

	grunt.loadNpmTasks('grunt-wp-i18n');
	grunt.loadNpmTasks('grunt-contrib-watch');

	// Tell Grunt what to do when we type "grunt" into the terminal.
	grunt.registerTask('dev', ['watch']);
	grunt.registerTask('cssonly', ['less', 'cssmin', 'autoprefixer']);
	// grunt.registerTask('jsonly', ['concat', 'uglify']);
    grunt.registerTask('jsonly', ['concat']);
	grunt.registerTask('default', ['cssonly', 'jsonly']);

};
