<?php
/**
 * Eventium functions and definitions
 *
 * @package Eventium
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'eventium_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function eventium_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Eventium, use a find and replace
	 * to change 'eventium' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'eventium', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );
	add_image_size( 'category-thumbnail', 300, 300, true );  //Imagen representativa de cada una de las categorías en front-page.php
	add_image_size( 'event-thumbnail', 160, 215, true );  //Imagen representativa de un evento en el listado en taxonomy-event-catgories.php
  add_image_size( 'event-full-image', 665, 442, true );  //Imagen representativa de un evento en single-event.php

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Menú principal', 'eventium' ),
		'footer' => __( 'Menú del pie', 'eventium' ),
	) );

	// Register Custom Navigation Walker
	require_once('inc/wp_bootstrap_navwalker.php');
	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	//add_theme_support( 'post-formats', array(
	//	'aside', 'image', 'video', 'quote', 'link',
	//) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'eventium_custom_background_args', array(
		'default-color' => 'ffffff',
		//'default-image' => '',
	) ) );
}
endif; // eventium_setup
add_action( 'after_setup_theme', 'eventium_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function eventium_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'eventium' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'sidebar para el blog', 'eventium' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => __( 'Destacados', 'eventium' ),
		'id'            => 'featured',
		'description'   => __('sidebar de recomendados', 'eventium'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s visible-sm-block visible-md-block visible-lg-block col-sm-12 col-md-6 col-lg-6"><div class="recommended-event">',
		'after_widget'  => '</div></aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => __( 'Sidebar del pie', 'eventium' ),
		'id'            => 'footer',
		'description'   => __('sidebar del pie', 'eventium'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s footer-sidebar col-sm-6 col-md-3 col-lg-3">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
}
add_action( 'widgets_init', 'eventium_widgets_init' );

// include "simple-widget.php";

/**
 * Enqueue scripts and styles.
 */
function eventium_scripts() {
  $version = wp_get_theme()->Version;

	wp_enqueue_style('material-design-icons', '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css', array(), $version);

	wp_enqueue_style( 'eventium-style', get_template_directory_uri() . '/assets/css/style.css', array(), $version);

	wp_enqueue_script( 'eventium-navigation', get_template_directory_uri() . '/assets/js/navigation.js', array(), $version, true );

	wp_enqueue_script( 'eventium-skip-link-focus-fix', get_template_directory_uri() . '/assets/js/skip-link-focus-fix.js', array(), $version, true );

	wp_enqueue_script( 'main-script', get_template_directory_uri() . '/assets/js/app.js', array('jquery'), $version, true);

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'eventium_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
//require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
//require get_template_directory() . '/inc/jetpack.php';

/* utilities */


function filter_search($query) {
  if ($query->is_search) {
  	// echo 'mierda2';
		$query->set('post_type', array('post', 'event'));
  };
  // echo '<pre>';
  // var_dump($query);
  // echo '</pre>';
  // die();
  return $query;
}
add_filter('pre_get_posts', 'filter_search');

/* $size string image size name */
function get_image_sizes( $size = '' ) {

  global $_wp_additional_image_sizes;

  $sizes = array();
  $get_intermediate_image_sizes = get_intermediate_image_sizes();

  // Create the full array with sizes and crop info
  foreach( $get_intermediate_image_sizes as $_size ) {

    if ( in_array( $_size, array( 'thumbnail', 'medium', 'large' ) ) ) {

      $sizes[ $_size ]['width'] = get_option( $_size . '_size_w' );
      $sizes[ $_size ]['height'] = get_option( $_size . '_size_h' );
      $sizes[ $_size ]['crop'] = (bool) get_option( $_size . '_crop' );

    } elseif ( isset( $_wp_additional_image_sizes[ $_size ] ) ) {

      $sizes[ $_size ] = array(
              'width' => $_wp_additional_image_sizes[ $_size ]['width'],
              'height' => $_wp_additional_image_sizes[ $_size ]['height'],
              'crop' =>  $_wp_additional_image_sizes[ $_size ]['crop']
      );

    }

  }

  // Get only 1 size if found
  if ( $size ) {

    if( isset( $sizes[ $size ] ) ) {
      return $sizes[ $size ];
    } else {
      return false;
    }

  }

  return $sizes;
}

if ( ! class_exists('Smk_ThemeView') ) {
    class Smk_ThemeView{
        private $args;
        private $file;

        public function __get($name) {
            return $this->args[$name];
        }

        public function __construct($file, $args = array()) {
            $this->file = $file;
            $this->args = $args;
        }

        public function __isset($name){
            return isset( $this->args[$name] );
        }

        public function render() {
            if( locate_template($this->file) ){
                include( locate_template($this->file) );//Theme Check free. Child themes support.
            }
        }
    }
}

if( ! function_exists('smk_get_template_part') ){
    function smk_get_template_part($file, $args = array()){
        $template = new Smk_ThemeView($file, $args);
        $template->render();
    }
}

/* CMB */

add_filter( 'cmb_meta_boxes', 'cmb_event_metaboxes' );

/**
 * Define the metabox and field configurations.
 *
 * @param  array $meta_boxes
 * @return array
 */
function cmb_event_metaboxes( array $meta_boxes ) {

  // Start with an underscore to hide fields from custom fields list
  $prefix = '_evm_';

  /**
   * Sample metabox to demonstrate each field type included
   */
  $meta_boxes['test_metabox'] = array(
    'id'         => 'test_metabox',
    'title'      => __( 'Información adicional', 'eventium' ),
    'pages'      => array('event'), // Post type
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true, // Show field names on the left
    // 'cmb_styles' => true, // Enqueue the CMB stylesheet on the frontend
    'fields'     => array(
      array(
          'name'      => __( '¿Es gratis?', 'eventium' ),
          'id'        => $prefix . 'is_free',
          'type'      => 'checkbox'
      ),
      array(
        'name'       => __( 'Tickets Online', 'eventium' ),
        'desc'       => __( 'Url de venta de tickets (opcional)' , 'eventium'),
        'id'         => $prefix . 'ticket_url',
        'type'       => 'text_url'
      ),
      array(
        'name'       => __( 'Venta de tickets', 'eventium' ),
        'desc'       => __( 'Venta física de tickets' , 'eventium'),
        'id'         => $prefix . 'ticket_locations',
        'type'       => 'text'
      ),
      array(
        'name'       => __( 'Precio', 'eventium' ),
        'desc'       => __( 'Texto libre (opcional)' , 'eventium'),
        'id'         => $prefix . 'prices',
        'type'       => 'text'
      ),
      array(
        'name'       => __( 'Edad recomendada', 'eventium' ),
        'desc'       => __( 'Introducir rango de edades con números (opcional)' , 'eventium'),
        'id'         => $prefix . 'age_range',
        'type'       => 'text'
      )
    )
  );

  // Add other metaboxes as needed

  return $meta_boxes;

}

/**
 * Initialize the metabox class.
 */
function cmb_initialize_cmb_meta_boxes() {

  if ( ! class_exists( 'cmb_Meta_Box' ) )
    require_once(plugin_dir_path( __FILE__ ) . 'inc/CMB/init.php');

}
add_action( 'init', 'cmb_initialize_cmb_meta_boxes', 9999 );

add_filter('sp_template_image-widget_widget.php', 'featured_events_filter');
function featured_events_filter($template) {
  return get_template_directory() . '/parts/featured-event-card.php';
}
