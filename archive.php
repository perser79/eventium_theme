<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Eventium
 */

get_header(); ?>

<?php
	global $wp_query;
	$day = $wp_query->query_vars['calendar_day'];
	$eventos = EM_Events::get(array('scope' => $day));
?>
<section class="main col-sm-8 col-md-7">
	<?php get_template_part('parts/action-bar'); ?>
	<section class="section-category-events">
<?php
	smk_get_template_part("list-events.php", array(
		'eventos' => $eventos,
	));
?>
	</section><!-- section-category -->
	
<?php get_footer(); ?>
