<?php
/**
 * Widget template. This template can be overriden using the "sp_template_image-widget_widget.php" filter.
 * See the readme.txt file for more info.
 */

// Block direct requests
if ( !defined('ABSPATH') )
  die('-1');

echo $before_widget;

if ( !empty( $title ) ) {
  echo $before_title;
  // title with anchor if provided
  if (!empty( $instance['link'] ) ) {
    $output = '';
    $attr = array(
      'href' => $instance['link'],
      'target' => $instance['linktarget'],
      'class' =>  'event-image-link',
      'title' => ( !empty( $instance['alt'] ) ) ? $instance['alt'] : $instance['title'],
    );
    $attr = apply_filters('image_widget_link_attributes', $attr, $instance );
    $attr = array_map( 'esc_attr', $attr );
    $output = '<a';
    foreach ( $attr as $name => $value ) {
      $output .= sprintf( ' %s="%s"', $name, $value );
    }
    $output .= '>';
    $output .= $title;
    $output .= '</a>';
    echo $output;
  } else {
    echo $title;
  }
  echo $after_title;
}

echo $this->get_image_html( $instance, true );

if ( !empty( $description ) ) {
  echo '<div class="'.$this->widget_options['classname'].'-description" >';
  echo wpautop( $description );
  echo "</div>";
}
echo $after_widget;
?>
