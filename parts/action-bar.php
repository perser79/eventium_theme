<div class="action-bar">
	<a href="#calendar-widget-sidebar" id="calendar-action-btn" class="circular-action-btn red-btn hidden-xs" data-toggle="collapse" aria-expanded="false" aria-controls="calendar-widget-sidebar"><i class="fa fa-calendar"></i></a>
	<?php get_template_part('searchform', 'events'); ?>
</div>
<div id="calendar-widget-sidebar" class="widget-area collapse" role="complementary" aria-expanded="true">
	<div id="em_calendar-2">
		<?php echo do_shortcode('[events_calendar long_events=0]'); ?>
	</div>
</div>
