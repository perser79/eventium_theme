<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package Eventium
 */

if ( ! is_active_sidebar( 'footer' ) ) {
	return;
}
?>

<section id="footer-widget-sidebar" class="widget-area" role="complementary">
	<div class="container">
		<div class="row">
			<?php dynamic_sidebar( 'footer' ); ?>
		</div>
	</div>
</section><!-- #secondary -->
