<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Eventium
 */

get_header(); ?>

<?php
  // $eventos = EM_Events::get(array(
  //   'category' => $term
  // ));
	$todos_eventos = EM_Events::get(array(
		'category' => $term
	)); /* Get events from plugin */
  /* En el día de hoy colocamos primero los eventos que sólo ocurren hoy y luego añadimos los eventos recurrentes, luego se añaden eventos posteriores*/
  $eventos_recurrentes = array();
  $eventos_unicos_hoy = array();
  $eventos_unicos_posteriores = array();
  $hoy = date('Y-m-d');
  $hoy_time = strtotime($hoy);
  foreach ($todos_eventos as $evento) {
    if ($evento->event_end_date && $evento->event_end_date != $evento->event_start_date && $evento->start < $hoy_time) {
      $eventos_recurrentes[] = $evento;
    } else {
      if ($evento->event_start_date == $hoy) {
        $eventos_unicos_hoy[] = $evento;
      } else {
        $eventos_unicos_posteriores[] = $evento;
      }
    }
  }
  $eventos = array_merge($eventos_unicos_hoy, array_reverse($eventos_recurrentes), $eventos_unicos_posteriores);
  // echo '<pre>';
  // var_dump($eventos);
  // echo '</pre>';
?>
<section class="main col-sm-8 col-md-7">
	<?php get_template_part('parts/action-bar'); ?>
	<section class="section-category-events">
<?php
  if (count($eventos)):
  	smk_get_template_part("list-events.php", array(
  		'eventos' => $eventos,
  	));
?>
<?php
  else:
?>
    <p>
      <?php echo __('No hemos encontrado eventos en esta categoría', 'eventium') ?>
    </p>
<?php
  endif;
?>
	</section><!-- section-category -->

<?php get_footer(); ?>
