<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package Eventium
 */

get_header(); ?>

<section class="main col-sm-8 col-md-7">	
	<?php get_template_part('parts/search-bar'); ?>
	<div class="error-404 not-found">
		<header class="page-header">
			<h1 class="page-title"><?php _e( '¡Huy! Esa página no se puede encontrar.', 'eventium' ); ?></h1>
		</header><!-- .page-header -->

		<div class="page-content">
			<p><?php _e( 'Parece que no se encontró nada. Prueba uno de los enlaces de abajo o una búsqueda', 'eventium' ); ?></p>

			<?php the_widget( 'WP_Widget_Recent_Posts' ); ?>

			<?php if ( eventium_categorized_blog() ) : // Only show the widget if site has multiple categories. ?>
			<div class="widget widget_categories">
				<h2 class="widget-title"><?php _e( 'Categorías más usadas', 'eventium' ); ?></h2>
				<ul>
				<?php
					wp_list_categories( array(
						'orderby'    => 'count',
						'order'      => 'DESC',
						'show_count' => 1,
						'title_li'   => '',
						'number'     => 10,
					) );
				?>
				</ul>
			</div><!-- .widget -->
			<?php endif; ?>

			<?php
				/* translators: %1$s: smiley */
				$archive_content = '<p>' . sprintf( __( 'Trate de buscar en los archivos mensuales. %1$s', 'eventium' ), convert_smilies( ':)' ) ) . '</p>';
				the_widget( 'WP_Widget_Archives', 'dropdown=1', "after_title=</h2>$archive_content" );
			?>

			<?php the_widget( 'WP_Widget_Tag_Cloud' ); ?>

		</div><!-- .page-content -->
	</div><!-- .error-404 -->
</section>


<?php get_footer(); ?>
