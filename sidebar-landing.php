<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package Eventium
 */

if ( ! is_active_sidebar( 'footer' ) ) {
	return;
}
?>

<section id="footer-widget-sidebar" class="widget-area" role="complementary">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-6 text-center feature-product">
				<div class="feature-product-icon">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/landing/responsive16.png" alt="responsive design icon">
				</div>
				<h3 class="title">Responsive<br>Design</h3>
			</div>
			<div class="col-md-4 col-sm-6 text-center feature-product">
				<div class="feature-product-icon">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/landing/smartphone103.png" alt="mobile first icon">
				</div>
				<h3 class="title">Mobile First<br>Design</h3>
			</div>
			<div class="col-md-4 col-sm-6 text-center feature-product">
				<span class="events-manager-icon"></span>
				<h3 class="title">Compatible con<br>Events Manager</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-sm-6 text-center feature-product">
				<div class="feature-product-icon">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/landing/column2.png" alt="multiple sidebars icon">
				</div>
				<h3 class="title">Multiple<br>Sidebars</h3>
			</div>
			<div class="col-md-4 col-sm-6 text-center feature-product">
				<div class="feature-product-icon" style="padding: 0 4px">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/landing/underscores.png" alt="underscores based icon">
				</div>
				<h3 class="title">Basado en<br>Underscores</h3>
			</div>
			<div class="col-md-4 col-sm-6 text-center feature-product">
				<div class="feature-product-icon">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/landing/website18.png" alt="underscores based icon">
				</div>
				<h3 class="title">Inspirado en<br>Material Design</h3>
			</div>
		</div>
	</div>
</section><!-- #secondary -->
