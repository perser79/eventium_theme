<?php
/**
 * The template for displaying search form.
 *
 * @package Eventium
 */
?>
<form role="search" action="<?php echo home_url( '/' ); ?>" class="search-form searchform">
	<input type="search" id="s" name="s" placeholder="<?php echo __('Buscar', 'eventium'); ?>" class="search-field" title="<?php echo esc_attr_x( 'Buscar:', 'label' ); ?>">
	<button type="submit" class="search-icon"><i class="flaticon-search100"></i></button>
</form>
