<?php
/**
 * Eventium Theme Customizer
 *
 * @package Eventium
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function eventium_customize_register( $wp_customize ) {
  $wp_customize->add_section('eventium_theme_options', array(
      'title'    => __('Opciones de eventium', 'eventium'),
      'description' => '',
      'type' => 'theme_mod',
      'priority' => 120,
  ));

  $wp_customize->add_setting( 'theme_claim' , array(
      'default'     => 'encuentra, comparte, disfruta<br><span>los eventos de tu ciudad</span>',
      'transport'   => 'refresh',
  ) );
  $wp_customize->add_control('themename_text_test', array(
      'label'      => __('Lema', 'themename'),
      'section'    => 'eventium_theme_options',
      'settings'   => 'theme_claim',
  ));
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
  $wp_customize->add_setting( 'themeslug_logo_complete' );
  $wp_customize->add_setting( 'themeslug_logo_reduced' );
  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'themeslug_logo_complete', array(
      'label'    => __( 'Logo completo', 'themeslug' ),
      'section'  => 'eventium_theme_options',
      'settings' => 'themeslug_logo_complete',
  ) ) );
  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'themeslug_logo_reduced', array(
      'label'    => __( 'Logo reducido', 'themeslug' ),
      'section'  => 'eventium_theme_options',
      'settings' => 'themeslug_logo_reduced',
  ) ) );
}
add_action( 'customize_register', 'eventium_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function eventium_customize_preview_js() {
	wp_enqueue_script( 'eventium_customizer', get_template_directory_uri() . 'assets/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', 'eventium_customize_preview_js' );
