<?php
/**
 * The template for displaying all single posts.
 *
 * @package Eventium
 */

get_header(); ?>

<?php

$evento = em_get_event(get_the_ID());
$location = new EM_Location($evento->location_id);
$datos_direccion = array('address', 'town', 'state');
$direccion = array();

foreach ($datos_direccion as $dato_direccion) {
	if (isset($location->$dato_direccion) && strlen($location->$dato_direccion)) {
		$direccion[] = $location->$dato_direccion;
	}
}
// echo '<pre>';
// var_dump($evento);
// echo '</pre>';
$objDateTime = new DateTime($evento->event_start_date . ' ' . $evento->event_start_time);
$isoDate = $objDateTime->format(DateTime::ISO8601);

$atributos = array(
	'_evm_is_free' => array('tipo' => 'checkbox', 'label' => __('Entrada gratuita', 'eventium')),
	'_evm_prices' => array('tipo' => 'text', 'label' => __('Precio entrada: ', 'eventium')),
	'_evm_ticket_locations' => array('tipo' => 'text', 'label' => __('Venta de tickets en: ', 'eventium')),
	'_evm_age_range' => array('tipo' => 'text', 'label' => __('Edad público destino: ', 'eventium'))
);

?>

<section class="main col-sm-8 col-md-7" data-post-id="<?php echo get_the_ID(); ?>">
	<?php get_template_part('parts/action-bar'); ?>

	<section class="section-single-event">
<?php
	$event_image_url = wp_get_attachment_image_src(get_post_thumbnail_id( $post->ID ), "event-full-image", true);
?>
		<img class="responsive" src="<?php echo $event_image_url[0]; ?>" alt="">
		<article>
			<div class="social collapse" id="collapse1">
				<?php echo do_shortcode ('[shareaholic app="share_buttons" id="290af8fc7432e1e18febf94ced726950"]'); ?>
			</div>
			<a class="share red-btn circular-action-btn" data-toggle="collapse" href="#collapse1" aria-expanded="false" aria-controls="collapse1"><i class="flaticon-share39"></i></a>
<?php
	$ticket_url = get_post_meta( get_the_ID(), '_evm_ticket_url', true );
	if ($ticket_url && strlen($ticket_url)) :
?>
			<a class="share red-btn circular-action-btn ticket" href="<?php echo $ticket_url; ?>" target="_blank"><i class="fa fa-fa fa-ticket"></i></a>
<?php
	endif;
?>
			<h4>
				<time datetime="<?php echo $evento->event_start_date . '' . $evento->event_start_time; ?>"><?php echo date_i18n('j F Y', $evento->start); ?></time>
<?php
	if ($evento->event_end_date && $evento->event_end_date != $evento->event_start_date):
?>
				- <time datetime="<?php echo $evento->event_start_date . '' . $evento->event_start_time; ?>"><?php echo date_i18n('j F Y', $evento->end); ?></time>
<?php
	endif;
?>
			</h4>	
<?php
	if (intval($evento->event_all_day) == 0):
?>		
			<h4>
				<time datetime="<?php echo $evento->event_start_date . '' . $evento->event_start_time; ?>"><?php echo date_i18n('H:i', $evento->start); ?>h</time>
<?php
		if ($evento->event_end_time && $evento->event_end_time != $evento->event_start_time):
?>
				- <time datetime="<?php echo $evento->event_end_date . '' . $evento->event_end_time; ?>"><?php echo date_i18n('H:i', $evento->end); ?>h</time>
<?php
		endif;
?>
			</h4>
<?php
	endif;
?>
<?php
  if ($location && intval($location->location_id) > 0):
?>
			<h4><a href="http://maps.google.com/maps?z=18&q=<?php echo $location->location_latitude; ?>,<?php echo $location->location_longitude; ?>" target="_blank"><i class="fa fa-map-marker"></i><?php echo $location->name; ?></span> - <span><?php echo $location->town; ?></a></h4>
<?php
	endif;
?>
			<h1><?php echo $evento->name; ?></h1>
			<?php echo $evento->post_content; ?>
			<hr>
<?php
	$es_gratis = get_post_meta( get_the_ID(), '_evm_is_free', true );
	$precio = get_post_meta( get_the_ID(), '_evm_prices', true );
	$ticket_locations = get_post_meta( get_the_ID(), '_evm_ticket_locations', true );
	if ($es_gratis) :
?>
			<h4><?php echo $atributos['_evm_is_free']['label']; ?></h4>
<?php
	else:
		if ($precio && strlen($precio)):
?>
			<h4><?php echo $atributos['_evm_prices']['label']; ?><?php echo $precio; ?></h4>
<?php
		endif;
		if ($ticket_locations && strlen($ticket_locations)):
?>
			<h4><?php echo $atributos['_evm_ticket_locations']['label']; ?><?php echo $ticket_locations; ?></h4>
<?php
		endif;
	endif;
	$rango_edades = get_post_meta( get_the_ID(), '_evm_age_range', true );
	if ($rango_edades && strlen($rango_edades)):
?>
			<h4><?php echo $atributos['_evm_age_range']['label']; ?><?php echo $rango_edades; ?></h4>
<?php
	endif;
?>
			<hr>
			<?php if (function_exists('rw_the_post_rating') ) { rw_the_post_rating(get_the_ID(), 'blog-post'); } ?>
		</article>
		<script type="application/ld+json">
		{
		  "@context": "http://schema.org",
		  "@type": "Event",
		  "name": "<?php echo $evento->name; ?>",
		  "startDate" : "<?php echo $isoDate; ?>",
		  "url" : "<?php echo get_permalink(); ?>",
		  "location" : {
		    "@type" : "Place",
		    "name" : "<?php echo $location->name; ?>",
		    "address" : "<?php echo implode(', ', $direccion); ?>"
		  }
		}
		</script>
		<article class="comentarios">
<?php
	// If comments are open or we have at least one comment, load up the comment template
	if ( comments_open() || get_comments_number() ) :
		comments_template();
	endif;
?>
		</article>
	</section>

<?php get_footer(); ?>
