<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package Eventium
 */

if ( ! is_active_sidebar( 'calendar-1' ) ) {
	return;
}
?>

<div id="calendar-widget-sidebar" class="widget-area collapse" role="complementary">
	<?php dynamic_sidebar( 'calendar-1' ); ?>
</div><!-- #secondary -->
