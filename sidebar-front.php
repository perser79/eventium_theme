<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package Eventium
 */

if ( ! is_active_sidebar( 'featured' ) ) {
	return;
}
?>

<section id="featured-widget-sidebar" class="widget-area visible-sm visible-md visible-lg" role="complementary">
	<h3><?php echo __('Te recomendamos', 'eventium'); ?></h3>
	<div class="row">
		<?php dynamic_sidebar( 'featured' ); ?>
	</div>
</section><!-- #secondary -->
